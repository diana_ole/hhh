window.addEventListener("DOMContentLoaded", function () {
    const PRICE_1 = 1850; //С†РµРЅР° РїРµСЂРІРѕРіРѕ Р°СЂРѕРјР°С‚Р°
    const PRICE_2 = 2400;//С†РµРЅР° РІС‚РѕСЂРѕРіРѕ Р°СЂРѕРјР°С‚Р°
    const PRICE_3 = 3000;//С†РµРЅР° С‚РµСЂС‚СЊРµРіРѕ Р°СЂРѕРјР°С‚Р°
    const EXTRA_PRICE = 0;
    const RADIO_1 = 1000; //РІС‹Р±РѕСЂ Сѓ РІС‚РѕСЂРѕРіРѕ Р°СЂРѕРјР°С‚Р°
    const RADIO_2 = 1500;
    const RADIO_3 = 500;
    const CHECKBOX = 350; //С‡РµРєР±РѕРєСЃ С‚СЂРµС‚СЊРµРіРѕ Р°СЂРѕРјР°С‚Р°
    let price = PRICE_1;
    let extraPrice = EXTRA_PRICE;
    let result;
    let calc = document.getElementById("calc"); 
    let myNum = document.getElementById("number"); //Р±РµСЂРµС‚ РєРѕР»-РІРѕ С‚РѕРІР°СЂР°
    let resultSpan = document.getElementById("result");// 
    let select = document.getElementById("select");// РѕРїСЂРµРґРµР»СЏРµС‚ РєР°РєРѕР№ С‚РѕРІР°СЂ РІС‹Р±СЂР°Р»Рё
    let radiosDiv = document.getElementById("radio-div");// Сѓ РІС‚РѕСЂРѕРіРѕ С‚РѕРІР°СЂР° РѕРїСЂРµРґРµСЏРµС‚ РІРёРґ , РІ РєРѕС‚РѕСЂРѕРј РІС‹Р±СЂР°Р»Рё
    let radios = document.querySelectorAll("#radio-div input[type=radio]");// РІРѕР·РІСЂР°С‰Р°РµС‚ РїРµСЂРІС‹Р№ СЌР»РµРјРµРЅС‚ ( Element ) РґРѕРєСѓРјРµРЅС‚Р°, РєРѕС‚РѕСЂС‹Р№ СЃРѕРѕС‚РІРµС‚СЃС‚РІСѓРµС‚ СѓРєР°Р·Р°РЅРЅРѕРјСѓ СЃРµР»РµРєС‚РѕСЂСѓ РёР»Рё РіСЂСѓРїРїРµ СЃРµР»РµРєС‚РѕСЂРѕРІ.
    let checkboxDiv = document.getElementById("check-div");// Р±РµСЂРµС‚ Сѓ 3 СЌР»РµРјРµРЅС‚Р°
    let checkbox = document.getElementById("checkbox");// РІС‹Р±СЂР°Р»Рё Р»Рё РјС‹ РѕС‚РєСЂС‹С‚РєСѓ 
    select.addEventListener("change", function (event) {// Р·Р°РїСѓСЃРєР°РµС‚ СЃРѕР±С‹С‚РёРµ РІС‹Р±РѕСЂ Р°СЂРѕРјР°С‚Р°
        let option = event.target;// СЃСЃС‹Р»РєР° РЅР° РѕР±СЉРµРєС‚, РєРѕС‚РѕСЂС‹Р№ СЋС‹Р» РёРЅРёС†РёР°С‚РѕСЂРѕРј СЃРѕР±С‹С‚РёСЏ
        if (option.value === "1") {//
            radiosDiv.classList.add("d-none");//
            checkboxDiv.classList.add("d-none");//
            extraPrice = EXTRA_PRICE;//
            price = PRICE_1;//
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");//
            checkboxDiv.classList.add("d-none");//
            extraPrice = RADIO_1;//
            price = PRICE_2;//
            document.getElementById("radio1").checked = true;//
        }
        if (option.value === "3") {//
            checkboxDiv.classList.remove("d-none");//
            radiosDiv.classList.add("d-none");//
            extraPrice = EXTRA_PRICE;//
            price = PRICE_3;//
            checkbox.checked = false;//
        }
    });
    radios.forEach(function (currentRadio) {//
        currentRadio.addEventListener("change", function (event) {//
            let radio = event.target;//
            if (radio.value === "r1") {//
                extraPrice = RADIO_1;//
            }
            if (radio.value === "r2") {//
                extraPrice = RADIO_2;//
            }
            if (radio.value === "r3") {//
                extraPrice = RADIO_3;//
            }
        });
    });
    checkbox.addEventListener("change", function () {//
        if (checkbox.checked) {
            extraPrice = CHECKBOX;//
        } 
        else {
            extraPrice = EXTRA_PRICE;//
        }
    });
    calc.addEventListener("change", function () {//
        if (myNum.value < 1) {
            myNum.value = 1;
        }
        result = (price + extraPrice) * myNum.value;//
        resultSpan.innerHTML = result;//
    });
});

